import {  useState } from "react";
import {  useDispatch, useSelector } from "react-redux";
import { createStore } from "redux";
const colorInit = "red";
const Reducer = (state = colorInit, action) => {
    switch (action.type) {
        case 'blue':
            return 'blue';
        default:
            return state;
    }
};

const Action = (color) => ({type: color});

export let store = createStore(Reducer);

function Task() {
    const [input, setInput] = useState("");
    const [task, setTask] = useState([]);
    const state = useSelector(state => state);
    const dispatch = useDispatch();
    const action = Action("blue");
    const onInputChange = (e) =>{
        setInput(e.target.value);
    }
    const AddTask = () => {
        if(input!==""){
           setTask([...task, input]);
           dispatch(action);
        }
    }
    const handleTask = (event) => {
        event.target.style.color = state;
    
    }
    return (
        <div>
            <input onChange={onInputChange}></input>
            <button onClick={AddTask}>Add Task</button>
            {task.map((value, index)=>{
             return   <p key={index} style={{color: "red"}} onClick = {handleTask}>{index + 1}. Task {index +1}</p>
            })}
        </div>
    )
};
export default Task;