import Task from "./components/Task";
import React from "react";

function App() {
  return (
    <div>
      <Task/>
    </div>
  );
}

export default App;
